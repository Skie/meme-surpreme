//Lewis Brown
//CST-361
//9-29-2019
//This assignment was completed in collaboration with Joe Leon, and Lewis Brown.
//We used source code from the following websites to complete this assignment:
//WEBSITE 1
//WEBSITE 2
package beans;

public class Twitter {
	
	String searchTerm;
	int numOfTweets;
	
	public Twitter(String searchTerm, int numOfTweets) {
		this.searchTerm = searchTerm;
		this.numOfTweets = numOfTweets;
	}
	
	public Twitter() {
		searchTerm = "";
		numOfTweets = 0;
	}
	
	public String getSearchTerm() {
		return searchTerm;
	}
	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}
	public int getNumOfTweets() {
		return numOfTweets;
	}
	public void setNumOfTweets(int numOfTweets) {
		this.numOfTweets = numOfTweets;
	}

}
