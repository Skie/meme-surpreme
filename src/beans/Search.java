package beans;

import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@Stateless
@ViewScoped
public class Search 
{

	private String Search;
	
	public Search()
	{
		Search = "Idaho";
	}
	public Search(String Search)
	{
		this.Search = Search;
	}

	public String getSearch() {
		return Search;
	}

	public void setSearch(String search) {
		Search = search;
	}
}
